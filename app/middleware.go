package app

import (
	"github.com/labstack/echo"
)

func SetUserStatus(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {

		if token, err := c.Cookie("session"); err == nil || (token != nil && token.Name != "") {
			c.Set("isLoggedIn", true)
		} else {
			c.Set("isLoggedIn", false)
		}

		return next(c)
	}
}

func EnsureLoggedIn(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {

		loggedIn, ok := c.Get("isLoggedIn").(bool)
		if !loggedIn || !ok {
			return c.Redirect(307, "/login")
		}

		return next(c)
	}
}

func EnsureNotLoggedIn(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {

		loggedIn, ok := c.Get("isLoggedIn").(bool)
		if loggedIn && ok {
			return c.Redirect(307, "/")
		}

		return next(c)
	}
}
