package main

import (
	"errors"
	"html/template"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/labstack/echo"
	"github.com/viknesh-nm/calculator/app"
)

type Template struct {
	templates *template.Template
}

func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

const (
	addOperator      = "+"
	subOperator      = "-"
	multiplyOperator = "*"
	divisionOperator = "/"
)

type Response struct {
	Name    string
	ErrType bool
	Error   string
	Answer  interface{}
}

var resp Response

var tempUser = map[string]string{
	"viknesh": "test123",
}

func main() {
	e := echo.New()
	// e.Use(middleware.Logger())
	t := &Template{
		templates: template.Must(template.ParseGlob("templates/*.html")),
	}

	e.Renderer = t
	e.Use(app.SetUserStatus)

	e.GET("/", func(c echo.Context) error {
		name, ok := c.Get("userName").(string)
		if ok {
			resp.Name = name
		}

		return c.Render(200, "calc", resp)
	}, app.EnsureLoggedIn)

	e.POST("/", func(c echo.Context) error {
		answer, err := Operation(c.FormValue("answer"))
		if err != nil {
			resp.ErrType = true
			resp.Error = err.Error()
		}

		name, ok := c.Get("userName").(string)
		if ok {
			resp.Name = name
		}
		resp.Answer = answer

		return c.Render(200, "calc", resp)
	}, app.EnsureLoggedIn)

	e.GET("/login", func(c echo.Context) error {
		return c.Render(200, "login", nil)
	}, app.EnsureNotLoggedIn)

	e.POST("/login", func(c echo.Context) error {
		name := c.FormValue("username")
		password := c.FormValue("password")

		if tempUser[name] == password && name != "" && password != "" {
			c.Set("userName", name)
			setCookie(c, name)
			return c.Redirect(301, "/")
		}

		resp := make(map[string]bool)
		resp["ErrType"] = true

		return c.Render(200, "login", resp)
	})

	e.Start(":8080")

}

func Operation(in string) (interface{}, error) {

	if strings.Contains(in, addOperator) {
		return addition(in)
	}

	if strings.Contains(in, subOperator) {
		return subtraction(in)
	}

	if strings.Contains(in, multiplyOperator) {
		return multiplication(in)
	}

	if strings.Contains(in, divisionOperator) {
		return division(in)
	}

	return 0, errors.New("Invalid Operation")
}

func addition(in string) (int64, error) {
	operands := strings.Split(in, addOperator)
	var sum int64
	if len(operands) < 2 {
		return 0, errors.New("Enter minimum two operands")
	}

	for _, e := range operands {
		if strings.Contains(e, multiplyOperator) || strings.Contains(e, subOperator) || strings.Contains(e, divisionOperator) {
			return 0, errors.New("Valid for only single operator")
		}

		aInt, _ := strconv.ParseInt(strings.TrimSpace(e), 0, 64)
		sum += int64(aInt)
	}

	return sum, nil
}

func subtraction(in string) (int64, error) {
	operands := strings.Split(in, subOperator)
	if len(operands) < 2 {
		return 0, errors.New("Enter minimum two operands")
	}

	sum, _ := strconv.ParseInt(strings.TrimSpace(operands[0]), 0, 64)

	for i := 1; i < len(operands); i++ {
		if strings.Contains(operands[i], multiplyOperator) || strings.Contains(operands[i], addOperator) || strings.Contains(operands[i], divisionOperator) {
			return 0, errors.New("Valid for only single operator")
		}

		aInt, _ := strconv.ParseInt(strings.TrimSpace(operands[i]), 0, 64)
		sum -= aInt
	}

	return sum, nil
}

func multiplication(in string) (float64, error) {
	operands := strings.Split(in, multiplyOperator)
	if len(operands) < 2 {
		return 0, errors.New("Enter minimum two operands")
	}

	var sum float64 = 1
	for _, e := range operands {
		if strings.Contains(e, addOperator) || strings.Contains(e, subOperator) || strings.Contains(e, divisionOperator) {
			return 0, errors.New("Valid for only single operator")
		}

		aInt, _ := strconv.ParseFloat(strings.TrimSpace(e), 64)
		sum *= aInt
	}

	return sum, nil
}

func division(in string) (float64, error) {
	operands := strings.Split(in, divisionOperator)
	if len(operands) != 2 {
		return 0, errors.New("Enter only two operands")
	}

	numerator, _ := strconv.ParseFloat(strings.TrimSpace(operands[0]), 64)

	if operands[1] == "0" {
		return 0, errors.New("Invalid operation (divide by 0)")
	}

	if strings.Contains(operands[1], addOperator) ||
		strings.Contains(operands[1], subOperator) ||
		strings.Contains(operands[1], multiplyOperator) {
		return 0, errors.New("Valid for only single operator")
	}

	denominator, _ := strconv.ParseFloat(strings.TrimSpace(operands[1]), 64)
	return numerator / denominator, nil
}

func setCookie(c echo.Context, name string) {
	cookie := new(http.Cookie)
	cookie.Name = "session"
	value, _ := app.Encrypt(name)
	cookie.Value = string(value)
	cookie.Expires = time.Now().Add(30 * time.Minute)
	c.SetCookie(cookie)
}
